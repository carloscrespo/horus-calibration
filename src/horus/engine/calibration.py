#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------#
#                                                                       #
# This file is part of the Horus Project                                #
#                                                                       #
# Copyright (C) 2014 Mundo Reader S.L.                                  #
#                                                                       #
# Date: March 2014                                                      #
# Author: Carlos Crespo <carlos.crespo@bq.com>                          #
#                                                                       #
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program. If not, see <http://www.gnu.org/licenses/>.  #
#                                                                       #
#-----------------------------------------------------------------------#

__author__ = u"Carlos Crespo <carlos.crespo@bq.com>"
__license__ = u"GNU General Public License v3 http://www.gnu.org/licenses/gpl.html"

import cv2
import numpy as np
import copy


class Calibration():
	"""Calibration class. For managing calibration"""
	def __init__(self, parent):
		self.matrix= [["fx",0,"cx"],[0,"fy","cy"],[0,0,1]]
		self.distortionVector=["k1","k2","p1","p2","k3"] 
		self.rotTransMatrix= [["rx","tx"],["ry","ty"],["rz","tz"]]
		# Parameters of the pattern
		self.patternRows=9 # points_per_column
		self.patternColumns=6 # points_per_row

		self.objp = np.zeros((self.patternRows*self.patternColumns,3), np.float32)
		self.objp[:,:2] = np.mgrid[0:self.patternColumns,0:self.patternRows].T.reshape(-1,2)
		self.objp=np.multiply(self.objp,12)
		self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 50, 0.001)
		self.imageStack= []
		self.objpoints= []
		self.imgpoints= []
		self.imgChessed=[]
		self.rvecs=[]
		self.tvecs=[]

	def pushImage (self, frame):
		self.imageStack.append(frame)

	def solvePnp(self):
		if not isinstance(self.matrix, np.ndarray):
			print "Camera matrix not found, using default"
			self.matrix=np.array([[  1.39809096e+03  , 0.00000000e+00 ,  4.91502299e+02], [  0.00000000e+00 ,  1.43121118e+03  , 6.74406283e+02], [  0.00000000e+00 ,  0.00000000e+00  , 1.00000000e+00]])
			self.distortionVector= np.array([ 0.11892648 ,-0.24087801 , 0.01288427 , 0.00628766 , 0.01007653])

		self.imgChessed=[]
		chessboardCount=0
		rvecs=np.array([])
		tvecs=np.array([])

		for index,img in enumerate(self.imageStack):
			self.objpoints=[]
			self.imgpoints=[]
			imgCopy=copy.deepcopy(img);
			gray= cv2.cvtColor(imgCopy,cv2.COLOR_BGR2GRAY)
			ret, corners = cv2.findChessboardCorners(gray, (self.patternColumns,self.patternRows),None)
			if ret == True:
				chessboardCount+=1
				self.objpoints.append(self.objp)
				
				cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),self.criteria)
				s=len(corners),2
				cornersCopy=np.zeros(s)
				for i,corner in enumerate(corners):
					cornersCopy.itemset((i,0),corner[0][0])
					cornersCopy.itemset((i,1),corner[0][1])
				self.imgpoints.append(cornersCopy)

				self.objpoints=np.array(self.objpoints)
				self.imgpoints=np.array(self.imgpoints)
				ret,rvecs,tvecs=cv2.solvePnP(self.objpoints,self.imgpoints,self.matrix,self.distortionVector)

				self.rvecs.append(rvecs)
				self.tvecs.append(tvecs)

			else:
				print "Chessboard not found"


	def calibrationFromImageStack(self):
		self.imgChessed=[]
		chessboardCount=0
		for img in self.imageStack:
			imgCopy=copy.deepcopy(img);
			gray= cv2.cvtColor(imgCopy,cv2.COLOR_BGR2GRAY)
			ret, corners = cv2.findChessboardCorners(gray, (self.patternColumns,self.patternRows),None)
			if ret == True:
				chessboardCount+=1
				self.objpoints.append(self.objp)
				cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),self.criteria)
				self.imgpoints.append(corners)
				cv2.drawChessboardCorners(imgCopy, (self.patternColumns,self.patternRows), corners,ret)
				self.imgChessed.append(imgCopy)
			else:
				print "Chessboard not found"
		ret, self.matrix, self.distortionVector, self.rvecs, self.tvecs = cv2.calibrateCamera(self.objpoints, self.imgpoints, gray.shape[::-1],None,None)

		print "---------------ret: ",ret

		h,  w = self.imageStack[0].shape[:2]
		self.newcameramtx, self.roi=cv2.getOptimalNewCameraMatrix(self.matrix,self.distortionVector,(w,h),1,(w,h))
		print "Camera matrix: ",self.matrix,type(self.matrix)
		print "Distortion coefficients: ", self.distortionVector
		print "Rotation matrix: ",self.rvecs,type(self.rvecs)
		print "Translation matrix: ",self.tvecs
		print "Chessboards found: ",str(chessboardCount)," out of ", str(len(self.imageStack))
		self.meanError()


	def meanError(self):
		mean_error = 0
		for i in xrange(len(self.objpoints)):
			imgpoints2, _ = cv2.projectPoints(self.objpoints[i], self.rvecs[i], self.tvecs[i], self.matrix, self.distortionVector)
			error = cv2.norm(self.imgpoints[i],imgpoints2, cv2.NORM_L2)/len(imgpoints2)
			mean_error += error
		print "total error: ", mean_error/len(self.objpoints)