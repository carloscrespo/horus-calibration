#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------#
#                                                                       #
# This file is part of the Horus Project                                #
#                                                                       #
# Copyright (C) 2014 Mundo Reader S.L.                                  #
#                                                                       #
# Date: March 2014                                                      #
# Author: Carlos Crespo <carlos.crespo@bq.com>                          #
#                                                                       #
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program. If not, see <http://www.gnu.org/licenses/>.  #
#                                                                       #
#-----------------------------------------------------------------------#

__author__ = u"Carlos Crespo <carlos.crespo@bq.com>"
__license__ = u"GNU General Public License v3 http://www.gnu.org/licenses/gpl.html"

import wx._core
import wx.lib.scrolledpanel

from horus.gui.control import *
from horus.gui.viewer import *

from horus.engine.scanner import *
from horus.engine.calibration import *

class MainWindow(wx.Frame):

    def __init__(self):
    	super(MainWindow, self).__init__(None, title="Horus-calibration",size=(1920,1080))

    	scanner = Scanner(self)
        
        calibration = Calibration(self)
        viewer = ViewNotebook(self, scanner,calibration)
        control = ControlNotebook(self, scanner, viewer,calibration)

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(control, 0, wx.ALL|wx.EXPAND, 10)
        sizer.Add(viewer, 1, wx.RIGHT|wx.TOP|wx.BOTTOM|wx.EXPAND, 10)
        self.SetSizer(sizer)
