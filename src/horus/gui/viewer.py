#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------#
#                                                                       #
# This file is part of the Horus Project                                #
#                                                                       #
# Copyright (C) 2014 Mundo Reader S.L.                                  #
#                                                                       #
# Date: March 2014                                                      #
# Author: Carlos Crespo <carlos.crespo@bq.com>                          #
#                                                                       #
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program. If not, see <http://www.gnu.org/licenses/>.  #
#                                                                       #
#-----------------------------------------------------------------------#

__author__ = u"Carlos Crespo <carlos.crespo@bq.com>"
__license__ = u"GNU General Public License v3 http://www.gnu.org/licenses/gpl.html"

import wx
import cv, cv2
import os
import matplotlib
matplotlib.interactive( True )
matplotlib.use( 'WXAgg' )
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg
from matplotlib.figure import Figure
import numpy as np
import random
import thread
from scipy import optimize
import matplotlib.cm as cm  
import matplotlib.colors as colors

class VideoTabPanel(wx.Panel):
    """
    """
    def __init__(self, parent,calibration):
        """"""
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)
        self.undistort=False
        self.showVideo=True
        self.calibration=calibration
        self.i=0

    def OnPaint(self, evt):
        
        dc = wx.BufferedPaintDC(self)
        dc.DrawBitmap(self.bmp, 0, 0)

    def refreshOn(self,fps):

        self.capture= cv2.VideoCapture(0)
        self.fps=fps
        
        
        ret, frame = self.captureImage()

        frameRes= cv2.resize(frame,(0,0),fx=0.72,fy=0.72)
        height, width = frameRes.shape[:2]
        self.bmp = wx.BitmapFromBuffer(width,height,frameRes)
        self.Bind(wx.EVT_PAINT,self.OnPaint)
        self.SetBackgroundColour(wx.RED)

        self.refreshTimer=wx.Timer(self)
        self.Bind(wx.EVT_TIMER,self.NextFrame,self.refreshTimer)     

        self.refreshTimer.Start(1000/fps)
        self.SetSize((width, height))

    def NextFrame(self, event):
        if (self.showVideo):      
            ret,frame = self.captureImage()           
            if ret:
                if(self.undistort):
                    h,  w = frame.shape[:2]
                    mapx,mapy = cv2.initUndistortRectifyMap(self.calibration.matrix,self.calibration.distortionVector,None,self.calibration.newcameramtx,(w,h),5)
                    dst = cv2.remap(frame,mapx,mapy,cv2.INTER_LINEAR)
                    dstRes= cv2.resize(dst,(0,0),fx=0.72,fy=0.72)
                    self.bmp.CopyFromBuffer(dstRes)
                else:
                    frameRes= cv2.resize(frame,(0,0),fx=0.72,fy=0.72)
                    self.bmp.CopyFromBuffer(frameRes)
                
        else:
            frame = self.staticFrame
            frameRes= cv2.resize(frame,(0,0),fx=0.72,fy=0.72)
            self.bmp.CopyFromBuffer(frameRes)

        self.Refresh()


    def captureImage(self):
        ret,frame=self.capture.read()
        frame=cv2.transpose(frame)
        frame=cv2.flip(frame,1)
        return ret,cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    def displayStaticImage(self,frame):
        self.staticFrame=frame
        self.i+=1
        coolFrame=cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        cv2.imwrite('pic{:>05}.png'.format(self.i),coolFrame)
        self.showVideo=False

    def displayVideo(self):
        self.showVideo=True

class MatPlotPanel(wx.Panel):
    """
    """
    def __init__(self, parent,calibration):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)
        self.calibration=calibration
        self.fig = Figure()
        self.canvas = FigureCanvasWxAgg( self, -1, self.fig)
        self.ax = self.fig.gca(projection='3d',axisbg=(0.1843, 0.3098, 0.3098))

        self.canvas.SetSize((1280, 960))

        self.X = np.arange(0, 12*5, 1)

        self.Y = np.arange(0, 12*8, 1)
        self.X, self.Y = np.meshgrid(self.X, self.Y)

        self.Xcopy=np.array([])
        self.Ycopy=np.array([])
        self.angles = np.linspace(0,360,100)[:-1]
        for xrow in self.X:
            self.Xcopy=  np.append(self.Xcopy,xrow)
        for yrow in self.Y:
            self.Ycopy=  np.append(self.Ycopy,yrow)
        self.Z = np.zeros(len(self.Xcopy))
        
        self.printCanvas()


    def printCanvas(self):

        axisXx,axisXy,axisXz=[0,50],[0,0],[0,0]
        axisYx,axisYy,axisYz=[0,0],[0,50],[0,0]
        axisZx,axisZy,axisZz=[0,0],[0,0],[0,50]
        self.ax.plot(axisXx,axisXy,axisXz,linewidth=3.0,color='red')
        self.ax.plot(axisYx,axisYy,axisYz,linewidth=3.0,color='blue')
        self.ax.plot(axisZx,axisZy,axisZz,linewidth=3.0,color='green')

        self.ax.set_xlabel('X')
        self.ax.set_ylabel('Z')
        self.ax.set_zlabel('Y')
        self.ax.set_xlim(-150, 150)
        self.ax.set_ylim(0, 300)
        self.ax.set_zlim(-150, 150)
        self.ax.invert_xaxis()
        self.ax.invert_yaxis()
        self.ax.invert_zaxis()
        # self.ax.view_init(elev=90., azim=180)


    def addToCanvas(self):
        self.rotation=self.calibration.rvecs
        self.translation=self.calibration.tvecs
        axisXx,axisXy,axisXz=[0,30],[0,0],[0,0]
        axisYx,axisYy,axisYz=[0,0],[0,30],[0,0]
        axisZx,axisZy,axisZz=[0,0],[0,0],[0,30]
        for ind,transvector in enumerate(self.rotation): 
            XPlane=np.zeros(len(self.Xcopy))
            YPlane=np.zeros(len(self.Xcopy))
            ZPlane=np.zeros(len(self.Xcopy))
            rtAxisXx,rtAxisXy,rtAxisXz=np.zeros(len(axisXx)),np.zeros(len(axisXy)),np.zeros(len(axisXz))
            rtAxisYx,rtAxisYy,rtAxisYz=np.zeros(len(axisYx)),np.zeros(len(axisYy)),np.zeros(len(axisYz))
            rtAxisZx,rtAxisZy,rtAxisZz=np.zeros(len(axisZx)),np.zeros(len(axisZy)),np.zeros(len(axisZz))
            rotTemp,jacob=cv2.Rodrigues(transvector)
            transTemp=self.translation[ind]

            for indPlane,pointPlane in enumerate(self.Xcopy):
                a=np.asarray([[self.Xcopy.item(indPlane)],[self.Ycopy.item(indPlane)],[self.Z.item(indPlane)]])

                rtPlane= (np.dot(rotTemp,a)+ transTemp)
                XPlane.itemset(indPlane,rtPlane.item(0))
                YPlane.itemset(indPlane,rtPlane.item(1))
                ZPlane.itemset(indPlane,rtPlane.item(2))
            for indAxis,pointAxis in enumerate( axisXx):
                bx= np.asarray([[axisXx[indAxis]],[axisXy[indAxis]],[axisXz[indAxis]]])
                rtAxisX=(np.dot(rotTemp,bx)+transTemp)
                rtAxisXx.itemset(indAxis,rtAxisX.item(0))
                rtAxisXy.itemset(indAxis,rtAxisX.item(1))
                rtAxisXz.itemset(indAxis,rtAxisX.item(2))
                by= np.asarray([[axisYx[indAxis]],[axisYy[indAxis]],[axisYz[indAxis]]])
                rtAxisY=(np.dot(rotTemp,by)+transTemp)
                rtAxisYx.itemset(indAxis,rtAxisY.item(0))
                rtAxisYy.itemset(indAxis,rtAxisY.item(1))
                rtAxisYz.itemset(indAxis,rtAxisY.item(2))
                bz= np.asarray([[axisZx[indAxis]],[axisZy[indAxis]],[axisZz[indAxis]]])
                rtAxisZ=(np.dot(rotTemp,bz)+transTemp)
                rtAxisZx.itemset(indAxis,rtAxisZ.item(0))
                rtAxisZy.itemset(indAxis,rtAxisZ.item(1))
                rtAxisZz.itemset(indAxis,rtAxisZ.item(2))
  
            self.ax.plot(rtAxisXx,rtAxisXz,rtAxisXy,linewidth=2.0,color='red')
            self.ax.plot(rtAxisYx,rtAxisYz,rtAxisYy,linewidth=2.0,color='green')
            self.ax.plot(rtAxisZx,rtAxisZz,rtAxisZy,linewidth=2.0,color='blue')
            self.ax.plot(XPlane, ZPlane, YPlane,color=(random.random(),random.random(),random.random(),0.5))
            self.canvas.draw()

    def clearPlot(self):
        self.ax.cla()
        self.printCanvas()
        self.canvas.draw()

class Plot2DPanel(wx.Panel):
    """
    """
    def __init__(self, parent,calibration):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)

        self.calibration=calibration
        self.fig = Figure(facecolor='white')
        self.canvas = FigureCanvasWxAgg( self, -1, self.fig)
        self.ax = self.fig.gca()
        self.ax.axis('equal')
        self.canvas.SetSize((1280, 960))
        self.x2D=np.array([])
        self.z2D=np.array([])

        # self.ax.title('Least Squares Circle')
        self.plot()
        print type(self.calibration.tvecs)

    def plot(self):
        self.ax.cla()
        print type(self.calibration.tvecs),self.calibration.tvecs
        if not self.calibration.tvecs:
            print "Translation data is empty, using training dataset"
            transVectors=np.array( [np.array([[ -42.69884629],
                   [ -60.66166513],
                   [ 321.66039025]]), np.array([[ -42.41493766],
                   [ -60.64227845],
                   [ 314.83533902]]),np.array([[ -41.50678247],
                   [ -60.61090239],
                   [ 309.86558147]]), np.array([[ -39.94829325],
                   [ -60.53994931],
                   [ 304.62751714]]), np.array([[ -35.74329096],
                   [ -60.37072737],
                   [ 295.90780527]]), np.array([[ -28.6653856 ],
                   [ -60.11288896],
                   [ 287.22736507]]), np.array([[ -17.04165965],
                   [ -59.83398485],
                   [ 280.00927257]]), np.array([[  -7.25630751],
                   [ -59.66790518],
                   [ 277.30374209]]), np.array([[   2.22381082],
                   [ -59.49892478],
                   [ 276.60780411]])])
        else:
            transVectors=self.calibration.tvecs
        self.x2D=np.array([])
        self.z2D=np.array([])
        for transUnit in transVectors:
               self.x2D=np.hstack((self.x2D,transUnit[0][0]))
               self.z2D=np.hstack((self.z2D,transUnit[2][0]))
        self.x2D=np.r_[self.x2D]
        self.z2D=np.r_[self.z2D]
        center_estimate = 0, 310
        print center_estimate
        center, ier = optimize.leastsq(self.f, center_estimate)

        xc, zc = center

        Ri     = self.calc_R(*center)
        R      = Ri.mean()
        residu = sum((Ri - R)**2)

        print xc,zc
        print R

        theta_fit = np.linspace(-np.pi, np.pi, 180)

        x_fit2 = xc + R*np.cos(theta_fit)
        z_fit2 = zc + R*np.sin(theta_fit)
        self.ax.plot(x_fit2, z_fit2, 'k--', lw=2)
        self.ax.plot([xc], [zc], 'gD', mec='r', mew=1)
        self.ax.set_xlabel('x')
        self.ax.set_ylabel('z')

        # plot the residu fields
        nb_pts = 100

        self.canvas.draw()
        xmin, xmax = self.ax.set_xlim()
        ymin, ymax = self.ax.set_ylim()

        vmin = min(xmin, ymin)
        vmax = max(xmax, ymax)

        xg, zg = np.ogrid[vmin-R:(vmin+4*R):nb_pts*1j, vmax-4*R:vmax+R:nb_pts*1j]
        xg = xg[..., np.newaxis]
        zg = zg[..., np.newaxis]

        Rig    = np.sqrt( (xg - self.x2D)**2 + (zg - self.z2D)**2 )
        Rig_m  = Rig.mean(axis=2)[..., np.newaxis]  
        residu = np.sum( (Rig-Rig_m)**2 ,axis=2)
        lvl = np.exp(np.linspace(np.log(residu.min()), np.log(residu.max()), 15))

        self.ax.contourf(xg.flat, zg.flat, residu.T, lvl, alpha=0.4, cmap=cm.Purples_r) # , norm=colors.LogNorm())
        
        # cbar = self.fig.colorbar(fraction=0.175, format='%.f')
        self.ax.contour (xg.flat, zg.flat, residu.T, lvl, alpha=0.8, colors="lightblue")
        # cbar.set_label('Residu') #TODO fix colorbar

        # plot data
        self.ax.plot(self.x2D, self.z2D, 'ro', label='data', ms=8, mec='b', mew=1)
        self.ax.legend(loc='best',labelspacing=0.1 )

        self.ax.set_xlim(xmin=vmin+R/5, xmax=vmin+2.6*R)
        self.ax.set_ylim(ymin=vmax-3*R, ymax=vmax)

        self.ax.grid()

        self.canvas.draw()


    def calc_R(self,xc, zc):
        return np.sqrt((self.x2D-xc)**2 + (self.z2D-zc)**2)

    def f(self,c):
        Ri = self.calc_R(*c)
        return Ri - Ri.mean()
class ViewNotebook(wx.Notebook):
    """
    """
    def __init__(self, parent, scanner,calibration):
        wx.Notebook.__init__(self, parent, id=wx.ID_ANY, style=wx.BK_DEFAULT)
        self.calibration=calibration
        self.scanner = scanner
        self.videoTabPanel = VideoTabPanel(self,calibration)
        self.matPlotPanel=MatPlotPanel(self,calibration)
        self.plot2DPanel=Plot2DPanel(self,calibration)

        self.AddPage(self.videoTabPanel, "Video Calibration",wx.EXPAND)
        self.AddPage(self.matPlotPanel, "Matplot",wx.EXPAND)
        self.AddPage(self.plot2DPanel,"Least squares",wx.EXPAND)
        self.SetSelection(2)

