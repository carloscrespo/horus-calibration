#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------#
#                                                                       #
# This file is part of the Horus Project                                #
#                                                                       #
# Copyright (C) 2014 Mundo Reader S.L.                                  #
#                                                                       #
# Date: March 2014                                                      #
# Author: Carlos Crespo <carlos.crespo@bq.com>                          #
#                                                                       #
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program. If not, see <http://www.gnu.org/licenses/>.  #
#                                                                       #
#-----------------------------------------------------------------------#

__author__ = u"Carlos Crespo <carlos.crespo@bq.com>"
__license__ = u"GNU General Public License v3 http://www.gnu.org/licenses/gpl.html"
    
import wx
import wx.lib.scrolledpanel

class CalibrationTabPanel (wx.lib.scrolledpanel.ScrolledPanel):
    def __init__(self, parent, scanner,viewer,calibration):
        """New Tab for calibrationPanel"""
        wx.lib.scrolledpanel.ScrolledPanel.__init__(self, parent=parent, id=wx.ID_ANY, size=(300,1080))
        
        self.displayIndex=0

        self.scanner = scanner
        self.viewer = viewer
        self.calibration = calibration

        self.connectionStaticText = wx.StaticText(self, label="Connection and manual calibration")
        self.photosTakenText=wx.StaticText(self,label="Photos Taken: ")
        self.photosTaken=wx.StaticText(self,label="0")

        self.connectButton = wx.ToggleButton(self,label="connect",size=(100,-1))
        self.connectButton.Bind(wx.EVT_TOGGLEBUTTON, self.connect)

        self.photoButton = wx.Button(self,label="Take photo",size=(100,-1))
        self.photoButton.Bind(wx.EVT_BUTTON,self.takePhoto)
        self.photoButton.Disable()

        self.emptyStackButton = wx.Button(self,label="EmptyStack",size=(100,-1))
        self.emptyStackButton.Bind(wx.EVT_BUTTON,self.emptyStack)

        self.toggleDistortionButton = wx.ToggleButton(self,label="Undistort",size=(100,-1))
        self.toggleDistortionButton.Bind(wx.EVT_TOGGLEBUTTON, self.toggleUndistort)
        self.toggleDistortionButton.Disable()

        self.solvePnpButton = wx.Button(self,label="SolvePnP",size=(100,-1))
        self.solvePnpButton.Bind(wx.EVT_BUTTON, self.solvePnp)
        self.solvePnpButton.Disable()

        self.autoCalibrationStaticText = wx.StaticText(self, label="Automatic calibration")
        
        self.manualCalibrationButton = wx.Button(self,label="Calibration",size=(100,-1))
        self.manualCalibrationButton.Bind(wx.EVT_BUTTON, self.manualCalibration)
        self.manualCalibrationButton.Disable();

        self.toggleChessedButton = wx.Button(self,label="Toggle chessed img",size=(100,-1))
        self.toggleChessedButton.Bind(wx.EVT_BUTTON, self.toggleChessBoard)
        self.toggleChessedButton.Disable();

        self.startButton = wx.ToggleButton(self,label="start",size=(100,-1))
        self.startButton.Bind(wx.EVT_TOGGLEBUTTON, self.fullCalibration)

        self.stopButton = wx.Button(self,label="stop",size=(100,-1))
        self.stopButton.Bind(wx.EVT_BUTTON, self.stopCalibration)
        self.stopButton.Disable();

        #camera matrix
        self.camMatrixStaticText = wx.StaticText(self, label="Camera matrix:")

        # self.matrix= [["fx",0,"cx"],[0,"fy","cy"],[0,0,1]] # TODO connect with scanner's calibration matrix

        self.visualMatrix=[[0 for j in range(len(self.calibration.matrix))] for i in range(len(self.calibration.matrix[0]))]
        for i in range(len(self.calibration.matrix)):
            for j in range (len(self.calibration.matrix[i])):
                self.visualMatrix[i][j]= wx.StaticText(self,label=str(self.calibration.matrix[i][j]))

        #distortion vector
        self.distortionCoeffStaticText = wx.StaticText(self, label="Distortion coefficients")


        self.visualDistortionVector=[0 for j in range(len(self.calibration.distortionVector))]
        for i in range(len(self.calibration.distortionVector)):
            self.visualDistortionVector[i]=wx.StaticText(self,label=str(self.calibration.distortionVector[i]))

        #rotation-traslation matrix
        self.rotTransStaticText = wx.StaticText(self,label="Rotation-translation matrix")

        self.rotTransMatrix= [["rx","tx"],["ry","ty"],["rz","tz"]] # TODO connect with scanner's rot-trans matrix
        
        self.visualRotTransMatrix=[[0 for j in range(len(self.rotTransMatrix[0]))] for i in range(len(self.rotTransMatrix))]
       
        for i in range(len(self.rotTransMatrix)):
            for j in range (len(self.rotTransMatrix[i])):
                self.visualRotTransMatrix[i][j]= wx.StaticText(self,label=str(self.rotTransMatrix[i][j]))
        #-- Layout

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(self.connectionStaticText, 0, wx.ALL, 10)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.connectButton,0,wx.ALL,10)
        hbox.Add(self.photoButton,0,wx.ALL,10)
        vbox.Add(hbox)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.photosTakenText,0,wx.ALL,10)
        hbox.Add(self.photosTaken,0,wx.ALL,10)
        hbox.Add(self.emptyStackButton,0,wx.ALL,10)
        vbox.Add(hbox)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.manualCalibrationButton,0,wx.ALL,10)
        hbox.Add(self.toggleChessedButton,0,wx.ALL,10)
        vbox.Add(hbox)


        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.toggleDistortionButton,0,wx.ALL,10)
        hbox.Add(self.solvePnpButton,0,wx.ALL,10)
        vbox.Add(hbox)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.autoCalibrationStaticText, 0, wx.ALL, 10)
        vbox.Add(hbox)
        

        hbox = wx.BoxSizer(wx.HORIZONTAL)   
        hbox.Add(self.startButton, 0, wx.ALL, 10)
        hbox.Add(self.stopButton, 0, wx.ALL, 10)
        vbox.Add(hbox)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.camMatrixStaticText,0,wx.ALL,10)
        vbox.Add(hbox)
        for i in range(len(self.visualMatrix)):
            hbox = wx.BoxSizer(wx.HORIZONTAL)  
            for j in range (len(self.visualMatrix[i])):
                hbox.Add(self.visualMatrix[i][j],0,wx.ALL,10)
            vbox.Add(hbox)

        hbox= wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.distortionCoeffStaticText,0,wx.ALL,10)
        vbox.Add(hbox)

        hbox= wx.BoxSizer(wx.HORIZONTAL)
        for i in range(len(self.visualDistortionVector)):
            hbox.Add( self.visualDistortionVector[i],0,wx.ALL,10)
        vbox.Add(hbox)

        hbox= wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(self.rotTransStaticText,0,wx.ALL,10)
        vbox.Add(hbox)
        for i in range(len(self.visualRotTransMatrix)):
            hbox = wx.BoxSizer(wx.HORIZONTAL)  
            for j in range (len(self.visualRotTransMatrix[i])):
                hbox.Add(self.visualRotTransMatrix[i][j],0,wx.ALL,10)
            vbox.Add(hbox)



        self.SetSizer(vbox)
        self.Centre()


    def fullCalibration(self, event):

        self.scanner.startCalibration();
        self.viewer.refreshOnlyCameraOn(200) 
        print 'calibration started!'
        
        self.stopButton.Enable()

    def toggleUndistort(self,event):
        if self.toggleDistortionButton.GetValue():
            self.viewer.videoTabPanel.undistort=True
        else:
            self.viewer.videoTabPanel.undistort=False

    def stopCalibration(self,event):
        print 'calibration stopped!'
        self.viewer.refreshOnlyCameraOff() # TODO Check exception
        self.startButton.SetValue(False)
        self.stopButton.Disable()
        self.scanner.stopCalibration();


    def connect(self,event):
        self.viewer.videoTabPanel.refreshOn(fps=10)
        self.photoButton.Enable()
        print "connecting..."

    def takePhoto(self,event):
        print "photo taken"
        ret,frame=self.viewer.videoTabPanel.captureImage()
        self.calibration.pushImage(frame)
        self.photosTaken.SetLabel(str(len(self.calibration.imageStack)))
        if len(self.calibration.imageStack)==1:
            self.manualCalibrationButton.Enable()
            self.toggleChessedButton.Enable()
            self.solvePnpButton.Enable()

    def manualCalibration(self,event):
        print "performing manual calibration"
        self.calibration.calibrationFromImageStack()
        self.reloadParameters()
        self.toggleDistortionButton.Enable()


    def emptyStack(self,event):
        del self.calibration.imageStack[:]
        del self.calibration.rvecs[:]
        del self.calibration.tvecs[:]
        self.viewer.matPlotPanel.clearPlot()
        self.photosTaken.SetLabel(str(len(self.calibration.imageStack)))

    def toggleChessBoard(self,event):
        if self.displayIndex< len(self.calibration.imgChessed):
            print self.displayIndex, "------",len(self.calibration.imgChessed)
            self.viewer.videoTabPanel.displayStaticImage(self.calibration.imgChessed[self.displayIndex])
            self.displayIndex+=1
        else:
            self.displayIndex=0
            self.viewer.videoTabPanel.displayVideo()

    def reloadParameters(self):
        for i in range(len(self.calibration.matrix)):
            for j in range (len(self.calibration.matrix[i])):
                self.visualMatrix[i][j].SetLabel(str(self.calibration.matrix[i][j]))

        for i in range(len(self.calibration.distortionVector)):
            self.visualDistortionVector[i].SetLabel(str(self.calibration.distortionVector[i]))

    def solvePnp(self,event):
        self.calibration.solvePnp()
        self.viewer.matPlotPanel.addToCanvas()
        self.viewer.plot2DPanel.plot()

class ControlNotebook(wx.Notebook):
    """ """
    def __init__(self, parent, scanner, viewer,calibration):
        wx.Notebook.__init__(self, parent, id=wx.ID_ANY, style=wx.BK_DEFAULT)
        calibrationPanel = CalibrationTabPanel(self, scanner, viewer,calibration)
        calibrationPanel.SetupScrolling()
        self.AddPage(calibrationPanel, "Calibration Panel")